class VidsController < ApplicationController
  before_action :set_vid, only: [:show, :edit, :update, :destroy]

  # GET /vids
  def index
    @vids = Vid.all
  end

  # GET /vids/1
  def show
  end

  # GET /vids/new
  def new
    @vid = Vid.new
  end

  # GET /vids/1/edit
  def edit
  end

  # POST /vids
  def create
    @vid = Vid.new(vid_params)

    if @vid.save
      redirect_to @vid, notice: 'Vid was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /vids/1
  def update
    if @vid.update(vid_params)
      redirect_to @vid, notice: 'Vid was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /vids/1
  def destroy
    @vid.destroy
    redirect_to vids_url, notice: 'Vid was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vid
      @vid = Vid.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def vid_params
      params.require(:vid).permit(:video, :string)
    end
end
