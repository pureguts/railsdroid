class VideoUploader < CarrierWave::Uploader::Base
   include CarrierWave::Video

  process encode_video: [:mp4, callbacks: { after_transcode: :set_success } ]
  
  version :standard do
    process :resize_to_fill => [100, 150, :north]
  end

  

end
