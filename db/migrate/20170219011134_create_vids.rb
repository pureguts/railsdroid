class CreateVids < ActiveRecord::Migration[5.0]
  def change
    create_table :vids do |t|
      t.string :video
      t.string :string

      t.timestamps
    end
  end
end
